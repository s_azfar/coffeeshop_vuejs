export const utility = {
  state: {
    loader: false,
    // sideNav:true
  },
  
  mutations: {
    setFireLoader(state) {
      state.loader =  !state.loader;
    },
    // setSideNav(state) {
    //   state.sideNav =  !state.sideNav;
    // },
  },

  actions: {
    fireLoader({ commit }) {
      commit("setFireLoader")
    }, 
    // fireNav({commit}){
    //   commit('setSideNav');
    // }   
  },
  
  getters :{
      getLoader: (state) => {
          return state.loader;
      },
    //   getSideNav: (state) => {
    //     return state.sideNav;
    // },
  }
};
