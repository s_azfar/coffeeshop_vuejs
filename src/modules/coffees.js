import coffee_api from "../api/coffeesapi";
import {Promise} from "es6-promise";

export const coffees = {
    state: {
        coffees: [],
        coffee: {},
        edit_coffee: {}
    },
    mutations: {
        setCoffees(state, coffees) {
            state.coffees = coffees;
        },

        setCoffee(state, coffee) {
            state.coffee = coffee;
        },

        setEditCoffee(state, edit_coffee) {
            state.edit_coffee = edit_coffee;
        }
    },
    actions: {
        loadCoffees({commit}) {
            commit("setFireLoader");
            coffee_api
                .getcoffees()
                .then(function (response) {
                    commit("setCoffees", response.data);
                    commit("setFireLoader");
                })
                .catch(function (error) {
                    console.log(error);
                });
        },




    postCoffee({ commit }, coffee) {
      new Promise((resolve, reject) => {
        coffee_api
          .savecoffee(coffee)
          .then(response => {

            setTimeout(() => {
              resolve(response);
            }, 1000)

          })
          .catch(error => {
            reject(error);
          });
      });
    },

    updateCoffee({ commit }, coffee) {
     return new Promise((resolve, reject) => {
        coffee_api
          .editcoffee(coffee.id, coffee)
          .then(response =>{
            setTimeout(() => {
              response.data.status ? resolve(response.data) :resolve({status:false});
            }, 1000)

          })
          .catch(error => {
            reject(error);
          });
      });
    },


        editConstructor({commit}, coffeeObject) {
            commit("setEditCoffee", coffeeObject);
        },

        deleteCoffee({commit}, coffeeid) {
            new Promise((resolve, reject) => {
                coffee_api
                    .removecoffee(coffeeid)
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        },

    },

    getters: {
        getLoadedCoffees(state) {
            return state.coffees;
        },
        getEditConstructor(state) {
            return state.edit_coffee;
        }
    }
};
