import category_api from "../api/categoryapi";
import { Promise } from "es6-promise";

export const categories = {
  state: {
    categories: [],
    edit_category:{}
  },

  mutations: {
    setCategories(state, categories) {
      state.categories = categories;
    },
    setEditCategory(state,edit_category){
      state.edit_category = edit_category;
    }
  },

  actions: {
    loadCategories({ commit }) {
      category_api
        .getCategoryDropdown()
        .then(function(response) {
          commit("setCategories", response.data);
        })
        .catch(function(error) {
          console.log(error);
        });
    },

    postCategory({ commit }, category) {
      new Promise((resolve, reject) => {
        category_api
          .saveCategory(category)
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    deleteCategory({ commit }, categoryid) {
      new Promise((resolve, reject) => {
        category_api
          .removeCategory(categoryid)
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    updateCategory({ commit }, category) {
      new Promise((resolve, reject) => {
        category_api
          .editCategory(category.id, category)
          .then(function(response) {
            resolve(response);
          })
          .catch(function(error) {
            reject(error);
          });
      });
    },

    editConstructor({commit},categoryObject){
      commit('setEditCategory',categoryObject);
    }

  },

  getters: {
    getLoadedCategories(state) {
      return state.categories;
    },
    getEditedConstructor(state){
      return state.edit_category;
    }
  }
};
