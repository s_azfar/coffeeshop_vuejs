import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../components/home.vue'
import coffees from '../components/coffee/coffees.vue'
import coffee from '../components/coffee/coffee.vue'
import settings from '../components/settings.vue'
import users from '../components/users.vue'
import categories from '../components/categories/categories.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: home
  },
  {
    path:'/coffees',
    name:'coffees',
    component:coffees
  },
  {
    path:'/categories',
    name:'categories',
    component:categories
  },
  {
    path:'/coffees/:id',
    name:'coffee',
    component:coffee
  },
  {
    path:'/settings',
    name:'settings',
    component:settings
  },
  {
    path:'/users',
    name:'users',
    component:users 
  },
 
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})

export default router
