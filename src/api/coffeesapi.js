import {COFFEE_CONFIG} from '../config.js';

export default {
    getcoffees: function () {
        // console.log("axios Working");
        return axios.get(COFFEE_CONFIG.API_URL + '/coffees');
    },

    getcoffee: function (coffeeid) {
        return axios.get(COFFEE_CONFIG.API_URL + '/coffees/' + coffeeid);
    },

    savecoffee: function (coffee) {
        return axios.post(COFFEE_CONFIG.API_URL + '/coffees', coffee, {
            // 'Content-Type': 'application/json'
            'Content-Type': 'multipart/form-data'
        })
    },

    editcoffee: function (coffeeid, coffee) {
        return axios.put(COFFEE_CONFIG.API_URL + '/coffees/' + coffeeid, coffee, {
            'Content-Type': 'application/json'
        })
    },

    removecoffee: function (coffeeid) {
        return axios.delete(COFFEE_CONFIG.API_URL + '/coffees/' + coffeeid)
    },


}
