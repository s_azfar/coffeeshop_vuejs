import {COFFEE_CONFIG} from '../config.js';

export default {

    getCategoryDropdown: function () {
        // console.log("axios Working");
        return axios.get(COFFEE_CONFIG.API_URL + '/categories/dropdown');
    },

    saveCategory: function (category) {
        return axios.post(COFFEE_CONFIG.API_URL +'/categories', category, {
            'Content-Type': 'application/json'
        })
    },
   
    removeCategory: function (category_id) {
        return axios.delete(COFFEE_CONFIG.API_URL+'/categories/' +category_id)
    },
  
    editCategory: function (category_id,formData){
        return axios.put(COFFEE_CONFIG.API_URL+'/categories/' +category_id,formData, {
           'Content-Type': 'application/json'
        })
    }




















}