require('es6-promise').polyfill();

import Vue from 'vue'
import Vuex from 'vuex'
import  {coffees}  from '../modules/coffees';
import {categories} from '../modules/category';
import {utility} from '../modules/utitlity'


Vue.use( Vuex );


export default new Vuex.Store({
    modules: { 
        
        coffees,
        categories,
        utility
    }
});